<?php

if (!defined('_PS_VERSION_'))
    exit;

class b2b extends Module
{
    protected $separator = '|';

    public function __construct()
    {
        $this->name = 'b2b';
        $this->tab = 'other';
        $this->version = '0.1';
        $this->author = 'skype: not_a_free_man';
        $this->need_instance = 0;
        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
        $this->bootstrap = true;

        parent::__construct();

        $this->displayName = $this->l('b2b import');
        $this->description = $this->l('Import from b2b service');
        $this->confirmUninstall = $this->l('Are you sure you want to uninstall?');
    }

    public function install()
    {
        if (!parent::install() ||
            !Configuration::updateValue('B2B_LINKS', ''))
            return false;

        return true;
    }

    public function uninstall()
    {
        if (!parent::uninstall())
            return false;

        return true;

    }

    public function renderForm()
    {
        $fields_form = array(
            'form' => array(
                'legend' => array(
                    'title' => $this->name,
                    'icon' => 'icon-link'
                ),
                'input' => array(
                    array(
                        'type' => 'text',
                        'label' => $this->l('Link'),
                        'name' => 'B2B_LINKS',
                        'class' => 'fixed-width-xs',
                    ),
                ),
                'submit' => array(
                    'name' => 'submit_' . $this->name,
                    'title' => $this->l('Save')
                )
            ),
        );

        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
        $this->fields_form = array();
        $helper->module = $this;
        $helper->identifier = $this->identifier;
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).
            '&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = array(
            'links' => $this->getLinks(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id
        );
        return $helper->generateForm(array($fields_form));
    }

    public function getContent()
    {
        $message = '';

        if (Tools::isSubmit('submit_' . $this->name)) {
            $newLink = Tools::getValue('B2B_LINKS');
            $links = $this->getLinks();
            if(!empty($newLink))
                $links[] = $newLink;

            if($this->setLinks($links))
                $message = $this->displayConfirmation($this->l("Success"));
            else
                $message = $this->displayError($this->l("Error saving"));
        } elseif(Tools::isSubmit('delete') && $this->ajax) {
            $id = Tools::getValue('id');

            if($this->deleteLinks($id))
                die(Tools::jsonEncode(array('hasError' => false, 'answer' => $this->displayConfirmation($this->l('Success delete')))));
            else
                die(Tools::jsonEncode(array('hasError' => true, 'answer' => $this->displayError($this->l('Error delete')))));
        }

        return $message.$this->renderForm();
    }

    public function getLinks()
    {
        $links = Configuration::get('B2B_LINKS');

        return explode($this->separator, $links);
    }

    public function setLinks($links)
    {
        $links = implode($this->separator, $links);

        return Configuration::updateValue('B2B_LINKS', $links);
    }

    public function deleteLinks($id)
    {
        $links = $this->getLinks();
        unlink($links[$id]);

        return $this->setLinks($links);
    }
}